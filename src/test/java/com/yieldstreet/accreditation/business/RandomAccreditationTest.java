package com.yieldstreet.accreditation.business;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.yieldstreet.accreditation.dto.AccreditationResponse;
import com.yieldstreet.accreditation.helper.BaseTest;
import com.yieldstreet.accreditation.util.AccreditationResponseBuilder;
import com.yieldstreet.accreditation.util.RandomIntGenerator;


@ExtendWith(SpringExtension.class)
class RandomAccreditationTest extends BaseTest {

    @Mock
    private RandomIntGenerator randomIntGenerator;

    @Mock
    private AccreditationResponseBuilder accreditationResponseBuilder;
    @InjectMocks
    private RandomAccreditation          randomAccreditation;

    @Test
    void randomAccreditationResultSucceedTest() {
        when(randomIntGenerator.getRandomInt()).thenReturn(1);
        when(accreditationResponseBuilder.getAccreditationResponseSucceed()).thenReturn(accreditationResponseSucceedBuilder());

        AccreditationResponse response = randomAccreditation.randomAccreditationResult();
        assertEquals(response, accreditationResponseSucceedBuilder());
    }

    @Test
    void randomAccreditationResultFailedTest() {
        when(randomIntGenerator.getRandomInt()).thenReturn(0);
        when(accreditationResponseBuilder.getAccreditationResponseFailed()).thenReturn(accreditationResponseFailedBuilder());
        AccreditationResponse response = randomAccreditation.randomAccreditationResult();
        assertEquals(response, accreditationResponseFailedBuilder());
    }
}