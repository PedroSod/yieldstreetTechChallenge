package com.yieldstreet.accreditation.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.yieldstreet.accreditation.helper.BaseTest;
import com.yieldstreet.accreditation.service.AccreditationService;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AccreditationController.class)
class AccreditationControllerTest extends BaseTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccreditationService accreditationService;

    @Test
    void userAccreditationSuccessTest() throws Exception {
        when(accreditationService.doAccreditation()).thenReturn(accreditationResponseSucceedBuilder());
        String request         = readFile(BASE_STUBS_PATH.concat("request.json"));
        String responseSuccess = readFile(BASE_STUBS_PATH.concat("success-response.json"));
        MvcResult result = mockMvc
            .perform(MockMvcRequestBuilders.post("/user/accreditation")
                                           .content(request)
                                           .contentType(MediaType.APPLICATION_JSON))
            .andReturn();
        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
        JSONAssert.assertEquals(response.getContentAsString(), responseSuccess, JSONCompareMode.STRICT);

    }

    @Test
    void userAccreditationFailTest() throws Exception {
        when(accreditationService.doAccreditation()).thenReturn(accreditationResponseFailedBuilder());
        String request         = readFile(BASE_STUBS_PATH.concat("request.json"));
        String responseSuccess = readFile(BASE_STUBS_PATH.concat("fail-response.json"));
        MvcResult result = mockMvc
            .perform(MockMvcRequestBuilders.post("/user/accreditation")
                                           .content(request)
                                           .contentType(MediaType.APPLICATION_JSON))
            .andReturn();
        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.OK.value(), response.getStatus());
        JSONAssert.assertEquals(response.getContentAsString(), responseSuccess, JSONCompareMode.STRICT);

    }

    @Test
    void userAccreditationBadRequestTest() throws Exception {
        when(accreditationService.doAccreditation()).thenReturn(accreditationResponseFailedBuilder());
        String request         = readFile(BASE_STUBS_PATH.concat("error-request.json"));
        String responseSuccess = readFile(BASE_STUBS_PATH.concat("error-response.json"));
        MvcResult result = mockMvc
            .perform(MockMvcRequestBuilders.post("/user/accreditation")
                                           .content(request)
                                           .contentType(MediaType.APPLICATION_JSON)).andReturn();

        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        JSONAssert.assertEquals(response.getContentAsString(), responseSuccess, JSONCompareMode.STRICT);

    }
}