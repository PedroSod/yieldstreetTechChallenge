package com.yieldstreet.accreditation.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.yieldstreet.accreditation.business.RandomAccreditation;
import com.yieldstreet.accreditation.dto.AccreditationRequest;
import com.yieldstreet.accreditation.dto.AccreditationResponse;
import com.yieldstreet.accreditation.helper.BaseTest;

@ExtendWith(SpringExtension.class)
class AccreditationServiceTest extends BaseTest {

    @Mock
    private RandomAccreditation randomAccreditation;

    @InjectMocks
    private AccreditationService accreditationService;

    @Test
    void doAccreditationFailedTest()  {
        when(randomAccreditation.randomAccreditationResult()).thenReturn(accreditationResponseFailedBuilder());
        AccreditationRequest  request  = accreditationRequestBuilder();
        AccreditationResponse response = accreditationService.doAccreditation();
        assertEquals(response, accreditationResponseFailedBuilder());

    }

    @Test
    void doAccreditationSucceedTest()  {
        when(randomAccreditation.randomAccreditationResult()).thenReturn(accreditationResponseSucceedBuilder());
        AccreditationRequest  request  = accreditationRequestBuilder();
        AccreditationResponse response = accreditationService.doAccreditation();
        assertEquals(response, accreditationResponseSucceedBuilder());

    }
}