package com.yieldstreet.accreditation.helper;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

import com.yieldstreet.accreditation.dto.AccreditationRequest;
import com.yieldstreet.accreditation.dto.AccreditationResponse;
import com.yieldstreet.accreditation.dto.Document;
import com.yieldstreet.accreditation.dto.Payload;

public abstract class BaseTest {

    public static final String BASE_STUBS_PATH = "src/test/resources/stubs/";

    protected String readFile(String relativeFilePath) throws Exception {
        return Files.readString(Path.of(relativeFilePath));
    }

    protected AccreditationResponse accreditationResponseSucceedBuilder() {
        return AccreditationResponse.builder()
                                    .accredited(true)
                                    .success(true)
                                    .build();
    }

    protected AccreditationResponse accreditationResponseFailedBuilder() {
        return AccreditationResponse.builder()
                                    .accredited(false)
                                    .success(false)
                                    .build();
    }

    protected AccreditationRequest accreditationRequestBuilder() {
        List<Document> documentList = Collections.singletonList(
            Document.builder()
                    .name("2018.pdf")
                    .mimeType("application/pdf")
                    .content("ICAiQC8qIjogWyJzcmMvKiJdCiAgICB9CiAgfQp9Cg==").build());

        Payload payload = Payload.builder()
                                 .accreditationType("BY_INCOME")
                                 .documents(documentList).build();

        return AccreditationRequest.builder()
                                   .userId("g8NlYJnk7zK9BlB1J2Ebjs0AkhCTpE1V")
                                   .payload(payload).build();

    }
}
