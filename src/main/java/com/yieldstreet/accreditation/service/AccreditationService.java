package com.yieldstreet.accreditation.service;

import org.springframework.stereotype.Service;

import com.yieldstreet.accreditation.business.RandomAccreditation;
import com.yieldstreet.accreditation.dto.AccreditationResponse;

@Service
public class AccreditationService {

    private RandomAccreditation randomAccreditation;

    public AccreditationService(RandomAccreditation randomAccreditation) {
        this.randomAccreditation = randomAccreditation;
    }

    public AccreditationResponse doAccreditation() {
        return randomAccreditation.randomAccreditationResult();
    }

}
