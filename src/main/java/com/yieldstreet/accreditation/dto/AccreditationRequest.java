package com.yieldstreet.accreditation.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonAlias;

@Getter
@Setter
@Builder
@EqualsAndHashCode
public class AccreditationRequest {

    @NotBlank(message = "user_id must not be null or blank!")
    @JsonAlias("user_id")
    private String  userId;
    @Valid
    private Payload payload;
}
