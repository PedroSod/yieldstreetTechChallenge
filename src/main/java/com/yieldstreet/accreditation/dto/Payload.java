package com.yieldstreet.accreditation.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonAlias;

@Getter
@Setter
@Builder
@EqualsAndHashCode
public class Payload {

    @Valid
    private List<Document> documents;
    @NotBlank(message = "accreditation_type must not be null or blank!")
    @JsonAlias("accreditation_type")
    private String         accreditationType;
}
