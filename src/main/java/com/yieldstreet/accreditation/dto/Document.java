package com.yieldstreet.accreditation.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonAlias;

@Getter
@Setter
@Builder
@EqualsAndHashCode
public class Document {

    @NotBlank(message = "name must not be null or blank!")
    private String name;
    @NotBlank(message = "mime_type must not be null or blank!")
    @JsonAlias("mime_type")
    private String mimeType;
    @NotBlank(message = "mime_type must not be null or blank!")
    private String content;

}
