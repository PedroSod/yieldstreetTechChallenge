package com.yieldstreet.accreditation.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yieldstreet.accreditation.dto.AccreditationRequest;
import com.yieldstreet.accreditation.dto.AccreditationResponse;
import com.yieldstreet.accreditation.service.AccreditationService;

@RequestMapping("/user/accreditation")
@RestController
public class AccreditationController {

    private AccreditationService accreditationService;

    public AccreditationController(AccreditationService accreditationService) {
        this.accreditationService = accreditationService;
    }

    @PostMapping
    public ResponseEntity<AccreditationResponse> userAccreditation(@Valid @RequestBody AccreditationRequest accreditation) {
        AccreditationResponse response = accreditationService.doAccreditation();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
