package com.yieldstreet.accreditation.business;

import org.springframework.stereotype.Component;

import com.yieldstreet.accreditation.dto.AccreditationResponse;
import com.yieldstreet.accreditation.util.AccreditationResponseBuilder;
import com.yieldstreet.accreditation.util.RandomIntGenerator;

@Component
public class RandomAccreditation {

    private RandomIntGenerator           randomIntGenerator;
    private AccreditationResponseBuilder accreditationResponseBuilder;

    public RandomAccreditation(RandomIntGenerator randomIntGenerator, AccreditationResponseBuilder accreditationResponseBuilder) {
        this.randomIntGenerator = randomIntGenerator;
        this.accreditationResponseBuilder = accreditationResponseBuilder;
    }

    public AccreditationResponse randomAccreditationResult() {

        if (isAccreditationSucceed()) {
            return accreditationResponseBuilder.getAccreditationResponseSucceed();
        }
        return accreditationResponseBuilder.getAccreditationResponseFailed();
    }

    private boolean isAccreditationSucceed() {
        return randomIntGenerator.getRandomInt() > 0;
    }

}
