package com.yieldstreet.accreditation.util;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class RandomIntGenerator {

    public int getRandomInt() {
        Random rand = new Random();
        return rand.nextInt(2);
    }

}
