package com.yieldstreet.accreditation.util;

import org.springframework.stereotype.Component;

import com.yieldstreet.accreditation.dto.AccreditationResponse;

@Component
public class AccreditationResponseBuilder {

    public AccreditationResponse getAccreditationResponseSucceed() {
        return AccreditationResponse.builder()
                                    .accredited(true)
                                    .success(true)
                                    .build();
    }

    public AccreditationResponse getAccreditationResponseFailed() {
        return AccreditationResponse.builder()
                                    .accredited(false)
                                    .success(false)
                                    .build();
    }
}
