User Accreditation at Yieldstreet:

- Javax was used to validate the JSON attributes;
- CustomExceptionHandler will deal with the exceptions, generating a JSON from ErrorResponse class;
- The AccreditationService doesn't do an accreditation, a random result is created in RandomAccreditation class;
- WebMvcTest and Mockito were used to write the unit tests;
